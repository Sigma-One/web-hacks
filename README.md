## Web Stuff

### Setup

#### Browser (Firefox)

| Name         | Details     |
| ------------ | ----------- |
| Stylus       | Userstyles  |
| GreaseMonkey | Userscripts |
